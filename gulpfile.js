var gulp = require('gulp');
var inject = require('gulp-inject');

gulp.task('default', function () {
    var target = gulp.src('./static/index.html');
    var sources = gulp.src(
        [
            './static/styles/base/reset.css',
            './static/styles/base/main.css',
            './static/styles/base/*.css',
            './static/styles/base/components/**/*.css',
            './static/styles/project/components/**/*.css',
            './static/styles/project/**/*.css'
        ],
        {read: false}
    );

    return target
        .pipe(inject(sources, {relative: true}))
        .pipe(gulp.dest('./static'));
});